{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards  #-}
module Bisnis.PembukuanPelanggan
  ( lihatPenggunaDanMeteran
  , tambahPengguna
  , lihatDaftarPelanggan
  , lihatPengguna
  , ubahDataPengguna
  , tambahPetugas
  , lihatDaftarPetugas
  )
where

import           Protolude

import           Database.Esqueleto
import           Data.Time

import           Conf
import           Model
import           Model.Grouping
import           Types

import           Bisnis.Otoritas
import           Bisnis.ValidasiData

import           Pertanyaan.TentangPengguna
import           Pertanyaan.TentangSistem

-- | Lihat daftar pengguna, meteran, dan apakah penggunaan
--   air pada tahun dan bulan saat ini sudah dicatat atau belum.
lihatPenggunaDanMeteran
  :: (MonadIO m, MonadReader Konfigurasi m, MonadError Gagal m)
  => Pengguna -- ^ Petugas.
  -> m [(Pengguna, Meteran, Bool)]
lihatPenggunaDanMeteran petugas = do
  Pengguna {..}     <- kewenanganMinimalPengguna petugas Petugas
  (tahun, bulan, _) <- toGregorian . utctDay <$> liftIO getCurrentTime
  pms               <- runDb $ selectPenggunaMeteranDanCatatMinum tahun bulan
  return $ map (\(p, m, s) -> (entityVal p, entityVal m, unValue s)) pms

lihatPengguna
  :: (MonadIO m, MonadReader Konfigurasi m, MonadError Gagal m)
  => Pengguna
  -> Text
  -> m ([Entity Pengguna], [Entity Grup])
lihatPengguna admin nomortelp = do
  _ <- kewenanganMinimalPengguna admin Admin
  runDb $ do
    pengguna <- selectPenggunaByNomorTelepon nomortelp
    grups    <- selectGrup
    return (pengguna, grups)

ubahDataPengguna
  :: (MonadIO m, MonadReader Konfigurasi m, MonadError Gagal m)
  => Pengguna
  -> Text
  -> Maybe Text
  -> Maybe Int64
  -> Maybe Text
  -> Maybe Text
  -> m [Entity Pengguna]
ubahDataPengguna admin notelp mnama mgid malamat mwil = do
  _ <- kewenanganMinimalPengguna admin Admin
  runDb $ do
    updatePengguna notelp mnama Nothing (toSqlKey <$> mgid) malamat mwil
    selectPenggunaByNomorTelepon notelp

-- | Menambah pengguna baru.
--   Menerima parameter sesuai dengan `RequestPenggunaBaru`.
--   Alasan menggunakan password dikarenakan mungkin akan
--   ada kebutuhan aplikas pengguna.
tambahPengguna
  :: (MonadIO m, MonadReader Konfigurasi m, MonadError Gagal m)
  => Pengguna -- ^ Admin.
  -> Text -- ^ Nama pengguna baru.
  -> Text -- ^ Nomor telepon pengguna baru.
  -> Text -- ^ Password.
  -> Text -- ^ Alamat pengguna.
  -> Text -- ^ Wilayah pengguna.
  -> Text -- ^ Nomor meteran.
  -> m (Entity Pengguna, Entity Meteran)
tambahPengguna admin nama telp pw alamat wilayah nometeran = do
  _ <- kewenanganMinimalPengguna admin Admin
  _ <- penggunaNil telp
  _ <- meteranHarusNil nometeran
  h <- utctDay <$> liftIO getCurrentTime
  x <- runDb $ do
    Entity pid _   <- insertPengguna nama telp pw Pelanggan alamat wilayah
    Entity _   met <- insertMeteran pid nometeran h
    selectPenggunaMeteran (Just $ meteranNomor met) Pelanggan
  case x of
    []    -> throwError $ GagalDB "meteran tidak ada" "saat masukkan pengguna"
    y : _ -> return y

tambahPetugas
  :: (MonadIO m, MonadReader Konfigurasi m, MonadError Gagal m)
  => Pengguna -- ^ Admin.
  -> Text -- ^ Nama pengguna baru.
  -> Text -- ^ Nomor telepon pengguna baru.
  -> Text -- ^ Password.
  -> m (Entity Pengguna, Entity Meteran)
tambahPetugas admin nama telp pw = do
  _ <- kewenanganMinimalPengguna admin Admin
  _ <- penggunaNil telp
  _ <- meteranHarusNil ("petugas-" <> telp)
  h <- utctDay <$> liftIO getCurrentTime
  x <- runDb $ do
    Entity pid _ <- insertPengguna nama
                                   telp
                                   pw
                                   Petugas
                                   ("alamat-petugas-" <> telp)
                                   ("wilayah-petugas-" <> telp)
    Entity _ met <- insertMeteran pid ("petugas-" <> telp) h
    selectPenggunaMeteran (Just $ meteranNomor met) Petugas
  case x of
    []    -> throwError $ GagalDB "petugas tidak ada" "saat memasukkan pengguna"
    y : _ -> return y

lihatDaftarPelanggan
  :: (MonadIO m, MonadReader Konfigurasi m, MonadError Gagal m)
  => Pengguna
  -> m [(Entity Pengguna, Entity Meteran)]
lihatDaftarPelanggan admin = do
  _ <- kewenanganMinimalPengguna admin Admin
  runDb $ selectPenggunaMeteran Nothing Pelanggan


lihatDaftarPetugas
  :: (MonadIO m, MonadReader Konfigurasi m, MonadError Gagal m)
  => Pengguna
  -> m [(Entity Pengguna, Entity Meteran)]
lihatDaftarPetugas admin = do
  _ <- kewenanganMinimalPengguna admin Admin
  runDb $ selectPenggunaMeteran Nothing Petugas
